import React from "react";

function DropDown(props){
  
  return(
      <label > {props.children} </label>       
      <select onChange ={props.ev}>
        {props.value.map(function(elem,index){
          return <option value={elem}>{elem}</option>;
        })
        }
      </select>
      <br/>
  );
}

export default DropDown;
