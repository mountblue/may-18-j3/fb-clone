import React from "react";

function Input(props){
  return(
    <React.Fragment>
      <input type="text" style={props.sty} placeholder={props.place} maxLength={props.len} name={props.name} id={props.id} className={props.class}/>
      <br />
    </React.Fragment>
  );
}

export default Input;
