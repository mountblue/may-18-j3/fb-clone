import React, { Component } from 'react';
import DropDown from './stateless/dropdown';
import Dash from './components/Dash';
import './App.css';
import myData from './assets/data.json';

class App extends Component {
  constructor(props) {
    super(props);
    this.state ={
      data:myData,
      select:"Images and Text"
    };
 }
 addCommentToFeed(e){
  if(e.target.parentElement.firstChild.value){
    var t1=(e.target.parentElement.firstChild.value);
    var t2=(new Date().toString());
    this.setState(prevState => ({
      comment: [...prevState.comment, t1],
      date: [...prevState.date, t2]
    }))
  }
  e.target.parentElement.firstChild.value="";
}

 handleLike(e){
  var l=this.state.like;
  if(e.target.innerHTML==="Like"){
    e.target.innerHTML="Dislike";
    this.setState({
      like:l+1
    });
  }
  else{
    e.target.innerHTML="Like";
    this.setState({
      like:l-1
    });
  }
}
  handleChange(e){
    if(e.target.value){
      var v=e.target.value;
    this.setState((state) => {
      return {select:v};
    });}
  }

  render() {
    return (
      <React-Fragment>
      <div className="main">
     <DropDown value={["Images and Text","Image Only","Text Only","No Items"]} ev={this.handleChange.bind(this)}>Select Posts with </DropDown>
     <Dash {...this.state} feedFunc={this.handleLike.bind(this)} commFunc={this.addCommentToFeed.bind(this)}/>
     </div>
     </React-Fragment>
    );
  }
}

export default App;
