import React, { Component } from 'react';
import Input from '../stateless/input';

class Comment extends Component {

  constructor(props) {
    super(props);
    this.state ={
      comment:[],
      date:[]
    };
 }

 componentDidMount(){
   var cur=this;
     cur.props.comment.forEach(function(com){
      var t1=com.comment;
      var t2=com.created_at;
      cur.setState(prevState => ({
        comment: [...prevState.comment, t1],
        date: [...prevState.date, t2]
      }))
  }); 
 }

  addCommentToFeed(e){
    if(e.target.parentElement.firstChild.value){
      var t1=(e.target.parentElement.firstChild.value);
      var t2=(new Date().toString());
      this.setState(prevState => ({
        comment: [...prevState.comment, t1],
        date: [...prevState.date, t2]
      }))
    }
    e.target.parentElement.firstChild.value="";
  }

  render() {
    var cur=this;
    return (
      <React-Fragment>
        <div className="comment">
          <h3>Comments</h3>
          {
            cur.state.comment.map(function(com,index){
              return (
                <React-Fragment>
                  <h2>{com}</h2>
                  <h6>{cur.state.date[index]}</h6>
                  <hr/>
                </React-Fragment>
              );
            })
          }
        </div>
        <div className="reply">
          <Input place="Add Comment" sty={{width:"100%"}}></Input><br/>
          <button onClick={this.addCommentToFeed.bind(this)}>Add</button>
        </div>
      </React-Fragment>
    );
  }
}

export default Comment;
