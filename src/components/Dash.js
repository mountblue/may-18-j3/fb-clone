import React, { Component } from 'react';
import Feed from './feed';
import Comment from './comment';
import './Dash.css';

class Dash extends Component {

  render() {
      var cur=this;
    return (
    <React-Fragment>
        <div className="dash">
            <h1>Feed</h1>
            {
                cur.props.select==="Text Only"?
                cur.props.data.map(function(elem){
                   if(elem.item_description!=="" && elem.image==="")
                   {
                       return(
                        <div id="feed">
                            <Feed liked={elem.isLiked} select={cur.props.select} {...elem}/>
                            <Comment comment={elem.comments}/>
                        </div>
                       );
                   }
                }):cur.props.select==="Image Only"?
                cur.props.data.map(function(elem){
                    if(elem.item_description==="" && elem.image!=="")
                    {
                        return(
                         <div id="feed">
                             <Feed liked={elem.isLiked} select={cur.props.select} {...elem}/>
                             <Comment comment={elem.comments}/>
                         </div>
                        );
                    }
                }):cur.props.select==="Images and Text"?
                cur.props.data.map(function(elem){
                    if(elem.item_description!=="" || elem.image!=="")
                    {
                        return(
                         <div id="feed">
                             <Feed liked={elem.isLiked} select={cur.props.select} {...elem}/>
                             <Comment comment={elem.comments}/>
                         </div>
                        );
                    }
                }):<img id="not" src="https://artlogic-res.cloudinary.com/h_1000,c_limit,f_auto,fl_lossy/artlogicstorage/lazinc/images/view/732be4352fff6f14c139be96c76b94cdj.jpg" alt="Nothing to See"></img>
            }       
        </div>
    </React-Fragment>
    );
  }
}

export default Dash;
