import React, { Component } from 'react';
import './Dash.css';


class Feed extends Component {
  
  constructor(props) {
    super(props);
    this.state ={
      like:this.props.likes
    };
 }

  handleLike(e){
    var l=this.state.like;
    if(e.target.innerHTML==="Like"){
      e.target.innerHTML="Dislike";
      this.setState({
        like:l+1
      });
    }
    else{
      e.target.innerHTML="Like";
      this.setState({
        like:l-1
      });
    }
  }

  render() {
    return (
      <React-Fragment>
        <div className="field">
          <h2>{this.props.item_description}</h2>
          <img src={this.props.image} ></img><br/>
          <i>{this.state.like} &#x1f44d;</i>
          <button onClick={this.handleLike.bind(this)}>Like</button>
        </div>
     </React-Fragment>
    );
  }
}

export default Feed;
