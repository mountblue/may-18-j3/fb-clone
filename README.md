1. Do not use props.children unless necessary, use props {...this.state}--> done

2. Avoid DOM manipulation in component methods to access input elements. use state and make the input elements as controlled components / use references

3. in Dash, remove the filtering logic, as its present in  Feed, improve the logic in Feed
--> removed filter logic in feed

4. Install eslint, and prettier-->done

5. Move the state of add comments and likes to App.js, and pass only functions to child components